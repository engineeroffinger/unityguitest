using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupTest : MonoBehaviour
{
    public Rect groupPos; // 父控件相对于游戏窗口的位置
    private void OnGUI()
    {
        #region 分组
        // 用于批量控制控件位置 
        // 可以理解为 包裹着的控件加了一个父对象 
        // 可以通过控制分组来控制包裹控件的位置
        GUI.BeginGroup(groupPos);
        // (0,0)是父控件内部的左上角
        GUI.Button(new Rect(0, 0, 100, 50), "测试按钮");
        GUI.Label(new Rect(0, 60, 100, 20), "Label信息");

        GUI.EndGroup();
        #endregion
    }
}
